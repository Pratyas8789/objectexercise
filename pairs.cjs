function pairs(obj) {
    if (typeof obj !== "object" || obj === null || Array.isArray(obj)) {
        return [];
    }
    let newObject = [];
    for (let key in obj) {
        newObject.push([key, obj[key]]);
    }
    return newObject;
}
module.exports = pairs;

