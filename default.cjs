function defaults(obj, defaultProps) {
    if (typeof obj === "string") {
        const tempObj = {}
        for (let index = 0; index < obj.length; index++) {
            tempObj[index] = obj.charAt(index);
        }
        obj = tempObj;
    }
    if (Array.isArray(obj) && obj !== []) {
        return obj;
    }

    if (obj === null ||  obj === undefined) {
        return defaultProps

    }
    if (typeof obj !== "object" || Array.isArray(obj)) {
        return [];
    }

    for (let key in defaultProps) {
        if (obj[key] === undefined) {
            obj[key] = defaultProps[key];
        }
    }
    return obj;
}
module.exports = defaults;