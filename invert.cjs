function invert(obj) {
    if (typeof obj !== "object" || obj === null) {
        return {};
    }
    let newObject = {};
    for (let key in obj) {
        newObject[obj[key]] = key;
    }
    return newObject;
}
module.exports = invert;