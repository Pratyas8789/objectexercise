function mapObject(obj, callBackFunction) {
    if (typeof obj !== "object" || obj === null || typeof callBackFunction !== "function") {
        return {};
    }
    let newObject = {};
    for (let key in obj) {
        newObject[key] = callBackFunction(obj[key], key);
    }
    return newObject;
}
module.exports = mapObject;