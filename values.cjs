function values(obj) {
    if (typeof obj !== "object" || obj === null) {
        return [];
    }
    let valueArray = [];
    for (let key in obj) {
        valueArray.push(obj[key]);
    }
    return valueArray;
}
module.exports = values;