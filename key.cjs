function keys(obj) {
    if (typeof obj !== "object" || obj === null) {
        return [];
    }
    let keyArray = [];
    for (let key in obj) {
        keyArray.push(key);
    }
    return keyArray;
}
module.exports = keys;